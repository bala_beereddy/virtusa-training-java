package exercise.solidprinciples.OCP.withOCP;

public class EmpOffer implements empDiscount{
    @Override
    public String offerManager() {
        //give your own logic
        String off="20% offer on birthday";
        return off;
    }

}
class PayDay implements empDiscount{
    @Override
    public String offerManager() {
        //give your own logic.
        String off="20% offer on payday";
        return off;
    }
}

