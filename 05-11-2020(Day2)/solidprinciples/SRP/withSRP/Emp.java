package SRP.withSRP;
//Created a seperate classes with setting and getting responisibilty
public class Emp {
    empInterface empInterface;
    String empname;
    int age;

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Emp(empInterface empInterface){
        this.empInterface=empInterface;
    }
    public void decoupledsearch(){
        this.empInterface.searchEmp();
    }

}
