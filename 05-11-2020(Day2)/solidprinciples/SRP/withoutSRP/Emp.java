package exercise.solidprinciples.SRP.withoutSRP;

//This has two responsibilities one is getting and setting emp information and other is searching employee,which violates Single Responsibility Principle..
public class Emp {
    String empname;
    String age;

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
    void searchemp(){

    }

}
