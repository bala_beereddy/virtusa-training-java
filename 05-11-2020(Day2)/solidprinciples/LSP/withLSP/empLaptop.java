package exercise.solidprinciples.LSP.withLSP;

public class empLaptop {
    String empname;
    String empaddress;
}
class officeLaptopRequired extends empLaptop{
    void deliveryAddress(){

    }
}
class officeLaptopToDifferentAddress extends empLaptop{
    void differentDeliveryAddress(){
     //get details of different address
    }
}
//Here we have the flexibility to ship to anywhere by just getting details of employee from parent class and use child classes to ship to locations as per requested.
