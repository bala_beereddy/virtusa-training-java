package exercise.solidprinciples.DIP.withDIP;

public class Manager {
    empWorker empWork;

    public void setEmpWorker(exercise.solidprinciples.DIP.withDIP.empWorker empWorker) {
        this.empWork = empWork;
    }
    public void manage(){
        empWork.work();
    }

}
