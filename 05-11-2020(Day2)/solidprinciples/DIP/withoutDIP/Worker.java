package exercise.solidprinciples.DIP.withoutDIP;

class Worker {
    public void work(){

    }
}
class SuperWorker{
    public void work(){

    }
}
class Manager{
    Worker worker;

    public void setWorker(Worker worker) {
        this.worker = worker;
    }
    public void manage(){
        worker.work();
    }
}
//Here we have to change manager class inorder to give functionality of SuperWorker.
