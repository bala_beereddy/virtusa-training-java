package exercise.solidprinciples.ISP.withoutISP;

public interface sponsership {
    void h1SponserShip();
    void gcSponserShip();
}
class h1emp implements sponsership{
    @Override
    public void h1SponserShip(){
        return;
    }

    @Override
    public void gcSponserShip() {
        return;
    }
}
//Here we don't need h1SponserShip for h1 employes.so we have to implement dummy h1sponsership method.
class optemp implements sponsership{
    @Override
    public void h1SponserShip() {
        return;
    }

    @Override
    public void gcSponserShip() {
        return;
    }
}
//Here opt students require both sponserships but h1 emp don't require h1 sponsership.Its violating ISP principle.