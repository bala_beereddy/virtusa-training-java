import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Q3 {
    public static String findValue(Map<String, String> map, String searchKey)
    {
       Optional<Map.Entry<String,String>> key=map.entrySet().stream().filter(a->a.getKey().equals(searchKey)).findAny();
       // System.out.println(key);
       if(key.isPresent())
           return map.get(searchKey);
       else
           return "Key not found";

    }
    public static void main(String[] args) {
        Map<String ,String> map=new HashMap<>();
        map.put("1","one");
        map.put("2","two");
        map.put("3","three");
        System.out.println(findValue(map,"4"));
    }
}
