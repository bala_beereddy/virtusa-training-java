
import java.util.ArrayList;
import java.util.List;

public class AverageOfElementsQ1 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.stream().mapToInt(i -> i).average().ifPresent(average->System.out.println(average));
    }
}

