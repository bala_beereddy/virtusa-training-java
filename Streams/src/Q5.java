import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Q5 {
    public static void main(String[] args) {
        List<String> output =new ArrayList<>();
        try {
            Stream<Path> pathStream= Files.walk(Paths.get("C:\\Users\\bbeereddy\\IdeaProjects\\Advance-java-assignment\\src\\folder"));
            pathStream.filter(Files::isRegularFile).forEach(file->{
                        Stream<String> stream=null;
                        try {
                            stream=Files.lines(file);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        stream.map(y->y.toUpperCase()).forEach(x->output.add(x));
                    }
                    );
            Files.write(Paths.get("Output.txt"),output, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
