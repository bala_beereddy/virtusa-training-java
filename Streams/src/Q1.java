import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Q1 {
    public static List<String> getFlattenList(List<List<String>>
                                                      listOfListOfString){
        List<String> l1= listOfListOfString.stream().flatMap(Collection::stream).collect(Collectors.toList());
        System.out.println(l1);
        return l1;

    }

    public static void main(String[] args) {
        List<String > list=new ArrayList<>();
        List<String > list2=new ArrayList<>();
        List<List<String>> listList=new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list2.add("4");
        list2.add("5");
        list2.add("6");
        listList.add(list);
        listList.add(list2);
        getFlattenList(listList);
    }
}
