import java.util.HashMap;
import java.util.Map;

public class PrintMapKeyValueQ3 {
    public static void printMapKeyValue(Map<String, String> map){
        map.forEach((i,j) -> System.out.println("KEY:"+i+" = VALUE:"+j));
    }
    public static void main(String[] args) {
        Map<String,String> map=new HashMap<>();
        map.put("1","one");
        map.put("2","two");
        map.put("3","three");
        printMapKeyValue(map);
    }
}
