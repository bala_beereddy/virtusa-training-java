
//given a List<String> with different Strings, write a function that returns a Map<Integer,List<String>> where Key is integer and value is a List of Strings containing all the Strings that has the length equivalent to the key.

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListAsValuesQ5 {

    public static Map<Integer, List<String>> function(List<String> list){
        Map<Integer,List<String>> map = new HashMap<>();
        for(String list1:list){
            int len = list1.length();
            if(map.containsKey(len)){
                List<String> nestedList =map.get(len);
                nestedList.add(list1);
                map.put(len,nestedList);
            }
            else{
                List<String> nestedList2=new ArrayList<>();
                nestedList2.add(list1);
                map.put(len,nestedList2);
            }
        }
        return map;
    }
    public static void main(String[] args) {

        List<String> list=new ArrayList<String>();
        list.add("a");
        list.add("e");
        list.add("pi");
        list.add("it");
        list.add("sky");
        list.add("fur");
        System.out.println(function(list));


    }
}
