import java.util.ArrayList;
import java.util.List;

public class AverageOfElementsQ1 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        int sum=0;
        for(int i:list){
            sum+=i;
        }
        int average=sum/ list.size();
        System.out.println("Average is "+average);
    }
}
