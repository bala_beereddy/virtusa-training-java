/*
You are given a folder with multiple files. Each file contains many lines each with a random String. Write a
program (a main method is fine) that reads all the lines in each of the files and convert them to
uppercase. Generate a single output file containing all the lines you converted already to uppercase.
Don't have to worry about the order.
*/

import java.io.*;
import java.util.Scanner;

public class ReadingFilesQ7 {
    public static void main(String[] args) {
        try{
            File folder = new File("C:\\Users\\bbeereddy\\IdeaProjects\\Advance-java-assignment\\src\\folder");
            File obj=new File("OutputFile.txt");
            obj.createNewFile();
            FileWriter myWriter=new FileWriter("outputFile.txt");
            for(File file:folder.listFiles()){
                BufferedReader bufferedReader=new BufferedReader(new FileReader(file));
                Scanner scanner=new Scanner(file);
                String line=null;
                while(scanner.hasNextLine()){
                    String data=scanner.nextLine();
                    myWriter.write(data.toUpperCase() + "\n");
                }
                scanner.close();
            }
            myWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
