import java.util.HashMap;
import java.util.Map;

public class SearchKeyQ4 {
    public static String findValue(Map<String, String> map, String searchKey)
    {
        if(map.containsKey(searchKey)){
            return map.get(searchKey);
        }
        else{
            return "NOT_FOUND";
        }
    }
    public static void main(String[] args) {
        Map<String,String> map=new HashMap<>();
        map.put("1","one");
        map.put("2","two");
        map.put("3","three");
        System.out.println(findValue(map,"4"));
    }
}
