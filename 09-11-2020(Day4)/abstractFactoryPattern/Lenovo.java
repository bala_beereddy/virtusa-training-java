package abstractFactoryPattern;

public class Lenovo extends Laptop {
    private int RAM;

    @Override
    public int getRAM() {
        return RAM;
    }

    @Override
    public String getProc() {
        return proc;
    }

    @Override
    public String getChipset() {
        return chipset;
    }

    private String proc;
    private String chipset;

    public Lenovo(int RAM, String proc, String chipset) {
        this.RAM = RAM;
        this.proc = proc;
        this.chipset = chipset;
    }
}
