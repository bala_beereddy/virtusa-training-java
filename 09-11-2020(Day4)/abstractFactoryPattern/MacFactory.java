package abstractFactoryPattern;

public class MacFactory implements LaptopAbstractFactory {

    public Laptop createLaptop() {
        return new Mac(2,"i3","dual core");
    }
}
