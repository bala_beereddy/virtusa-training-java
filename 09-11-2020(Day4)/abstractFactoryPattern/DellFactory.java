package abstractFactoryPattern;

public class DellFactory implements LaptopAbstractFactory {
    @Override
    public Laptop createLaptop() {
        return new Dell(4,"i5","quad core");
    }
}
