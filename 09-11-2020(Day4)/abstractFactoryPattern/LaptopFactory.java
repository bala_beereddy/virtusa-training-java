package abstractFactoryPattern;

public class LaptopFactory {
    public static Laptop getLaptop(LaptopAbstractFactory factory){
        return factory.createLaptop();
    }
}
