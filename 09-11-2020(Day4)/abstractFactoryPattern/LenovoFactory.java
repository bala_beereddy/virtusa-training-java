package abstractFactoryPattern;

public class LenovoFactory implements LaptopAbstractFactory {
    @Override
    public Laptop createLaptop() {
        return new Lenovo(8,"i7","multicore");
    }
}
