package abstractFactoryPattern;

public class Mac extends Laptop {
    private int RAM;
    private String proc;
    private String chipset;

    @Override
    public int getRAM() {
        return RAM;
    }

    public String getProc() {
        return proc;
    }

    public String getChipset() {
        return chipset;
    }



    public Mac(int RAM, String proc, String chipset) {
        this.RAM = RAM;
        this.proc = proc;
        this.chipset = chipset;
    }
}
