package abstractFactoryPattern;

public interface LaptopAbstractFactory {
    public Laptop createLaptop();
}
