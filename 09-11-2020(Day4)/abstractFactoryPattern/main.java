package abstractFactoryPattern;

public class main {
    public static void main(String[] args) {
        MacFactory macFactory=new MacFactory();
 Laptop mac = macFactory.createLaptop();
    }
}
