package abstractFactoryPattern;

public class Dell extends Laptop {
    private int RAM;
    private String proc;
    private String chipset;

    public Dell(int RAM, String proc, String chipset) {
        this.RAM = RAM;
        this.proc = proc;
        this.chipset = chipset;
    }

    @Override
    public int getRAM() {
        return RAM;
    }

    @Override
    public String getProc() {
        return getProc();
    }

    @Override
    public String getChipset() {
        return getChipset();
    }
}
