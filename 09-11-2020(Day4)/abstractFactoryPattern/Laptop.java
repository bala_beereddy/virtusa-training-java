package abstractFactoryPattern;

public abstract class Laptop {
    public abstract int getRAM();
    public abstract String getProc();
    public abstract String getChipset();
}
