public class Mac implements Laptop {
    private int RAM;
    private String proc;
    private String chipset;

    public Mac(int RAM, String proc, String chipset) {
        this.RAM = RAM;
        this.proc = proc;
        this.chipset = chipset;
    }
}
