public class Dell implements Laptop {
    private int RAM;
    private String proc;
    private String chipset;

    public Dell(int RAM, String proc, String chipset) {
        this.RAM = RAM;
        this.proc = proc;
        this.chipset = chipset;
    }
}
