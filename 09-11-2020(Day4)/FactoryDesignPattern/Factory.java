public class Factory {
    private Factory(){

    }
    public static Laptop createObject(String type){
        if(type.equals(Laptop.MAC)) return new Mac(2, "i3", "dualcore");
        else if(type.equals(Laptop.DELL)) return new Dell(4,"i5","quadcore");
        else if(type.equals(Laptop.LENOVO)) return new Lenovo(8,"i7","multicore");
        else return null;

    }
}
