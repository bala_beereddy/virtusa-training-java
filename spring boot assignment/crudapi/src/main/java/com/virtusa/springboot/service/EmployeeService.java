package com.virtusa.springboot.service;

import java.util.List;

import com.virtusa.springboot.model.Employee;

public interface EmployeeService {
	
	List<Employee> get();
	
	Employee get(int id);
	
	void save(Employee employee);
	
	void delete(int id); 
}
