package com.virtusa.springboot.dao;

import java.util.List;

import com.virtusa.springboot.model.Employee;

public interface EmployeeDAO {
	
	List<Employee> get();
	
	Employee get(int id);
	
	void save(Employee employee);
	
	void delete(int id); 
}
