package com.virtusa.AOP.Service;

import com.virtusa.AOP.Model.Student;

public class StudentService {
    private Student student;


    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
