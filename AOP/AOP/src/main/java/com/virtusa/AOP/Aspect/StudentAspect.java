package com.virtusa.AOP.Aspect;

import org.aspectj.lang.annotation.Before;

public class StudentAspect {
    @Before("execution(public String getName())")
    public void getNameAdvice(){
        System.out.println("Advice from getName()");
    }
    @Before("execution(*.com.virtusa.Service.*.get*())")
    public void getAllAdvice(){
        System.out.println("Service getters through aspect");
    }
}
