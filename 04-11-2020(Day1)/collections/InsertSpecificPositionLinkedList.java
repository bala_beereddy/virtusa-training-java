

import java.util.LinkedList;

public class InsertSpecificPositionLinkedList {
    public static void main(String[] args) {
        LinkedList linkedList=new LinkedList();
        linkedList.add("one");
        linkedList.add("two");
        linkedList.add("three");
        linkedList.add(1,"inserted");
        System.out.println(linkedList);
    }
}
