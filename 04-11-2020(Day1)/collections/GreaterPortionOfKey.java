import java.util.TreeMap;

public class GreaterPortionOfKey {
    public static void main(String[] args) {
        TreeMap treeMap=new TreeMap();
        treeMap.put(1,"number1");
        treeMap.put(2,"number2");
        treeMap.put(3,"number3");
        System.out.println("Keys greater than 2 :"+treeMap.tailMap(2,false));
    }
}
