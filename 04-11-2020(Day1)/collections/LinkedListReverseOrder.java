
import java.util.Iterator;
import java.util.LinkedList;

public class LinkedListReverseOrder {
    public static void main(String[] args) {
        LinkedList linkedList=new LinkedList();
        linkedList.add("one");
        linkedList.add("two");
        linkedList.add("three");
        Iterator iterator=linkedList.descendingIterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
