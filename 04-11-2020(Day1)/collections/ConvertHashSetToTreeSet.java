
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class ConvertHashSetToTreeSet {
    public static void main(String[] args) {
        Set hobj = new HashSet();
        hobj.add("one");
        hobj.add("plus");
        hobj.add("three");
        Set tobj = new TreeSet(hobj);
        System.out.println(tobj);

    }
}
