
import java.util.ArrayList;
import java.util.Collections;

public class CopyArrayList {
    public static void main(String[] args) {
        ArrayList arrayList=new ArrayList();
        arrayList.add("one");
        arrayList.add("two");
        arrayList.add("three");

        ArrayList arrayList1=new ArrayList();
        arrayList1.add("four");
        arrayList1.add("five");
        arrayList1.add("six");

        Collections.copy(arrayList1,arrayList);
        System.out.println(arrayList1);
    }
}
