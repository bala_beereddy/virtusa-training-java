
import java.util.TreeSet;

public class CompareTwoTreeSets {
    public static void main(String[] args) {
        TreeSet treeSet=new TreeSet();
        treeSet.add("one");
        treeSet.add("two");
        treeSet.add("three");
        TreeSet treeSet1=new TreeSet();
        treeSet1.add(1);
        treeSet1.add(2);
        treeSet1.add(3);
        boolean b=treeSet.equals(treeSet1);
        System.out.println(b);


        //Tree set which is strictly less than given element
        System.out.println(treeSet1.lower(3));
    }
}
