import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.PriorityQueue;

public class PriorityQueueToArray {
    public static void main(String[] args) {
        PriorityQueue priorityQueue=new PriorityQueue();
        priorityQueue.add("one");
        priorityQueue.add("two");
        priorityQueue.add("three");
        Object[] arrays = priorityQueue.toArray();
        System.out.println(Arrays.toString(arrays));
    }
}
