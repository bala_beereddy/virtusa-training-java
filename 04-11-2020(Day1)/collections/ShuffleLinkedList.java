

import java.util.Collections;
import java.util.LinkedList;

public class ShuffleLinkedList {
    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();
        linkedList.add("one");
        linkedList.add("two");
        linkedList.add("three");
        Collections.shuffle(linkedList);
        System.out.println(linkedList);
    }

}
