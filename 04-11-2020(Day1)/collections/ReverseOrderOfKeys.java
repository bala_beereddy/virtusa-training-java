import java.util.HashMap;
import java.util.NavigableMap;
import java.util.TreeMap;

public class ReverseOrderOfKeys {
    public static void main(String[] args) {
        TreeMap treeMap=new TreeMap();
        treeMap.put(1,"one");
        treeMap.put(2,"two");
        treeMap.put(3,"third");
        NavigableMap navigableMap=treeMap.descendingMap();
        System.out.println(navigableMap);

    }
}
