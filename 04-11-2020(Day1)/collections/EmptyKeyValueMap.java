import java.util.HashMap;

public class EmptyKeyValueMap {
    public static void main(String[] args) {
        HashMap hashMap = new HashMap();
        hashMap.put(1,"number1");
        hashMap.put(2,"number2");
        hashMap.put(3,"number3");
        if(hashMap.isEmpty()){
            System.out.println("empty");
        }
        else{
            System.out.println("not empty");
        }
    }
}
