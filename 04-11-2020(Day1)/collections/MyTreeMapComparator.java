
import java.util.Comparator;
import java.util.TreeMap;

public class MyTreeMapComparator {
    public static void main(String[] args){
        TreeMap<String,String > tm=new TreeMap<String, String>(new MyComp());
        tm.put("tommy","Hilfiger");
        tm.put("calvin","klein");
        tm.put("mike","cors");
        tm.put("kate","spade");
        System.out.println(tm);
    }
}
class MyComp implements Comparator<String>{
    public int compare(String str1,String str2){
        return str1.compareTo(str2);
    }
}
