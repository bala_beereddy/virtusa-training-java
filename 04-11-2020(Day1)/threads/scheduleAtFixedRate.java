//2
import java.util.Timer;
import java.util.TimerTask;
class scheduleAtFixedRate {
Timer timer;
    class scheduleAt extends TimerTask {

        @Override
        public void run() {
            long time = System.currentTimeMillis();
            if (time - scheduledExecutionTime() > 5) {
                return;
            } else {
                System.out.println("Time up!");
                timer.cancel();
            }

        }
    }

    class main {
        public void main(String[] args) {
            Timer timer = new Timer();
            System.out.println("Task Started");
            timer.scheduleAtFixedRate(timer,0,1*1000);
            System.out.println("Task Scheduled");

        }
    }
}