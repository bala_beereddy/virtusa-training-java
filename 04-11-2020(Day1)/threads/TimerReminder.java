//practice
import java.util.Timer;
import java.util.TimerTask;

public class TimerReminder {

    Timer timer;

    public TimerReminder() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new RemindTask(),0,1*1000);
    }

    class RemindTask extends TimerTask {
        public void run() {
            long time = System.currentTimeMillis();
            if (time - scheduledExecutionTime() > 5) {
                return;
            }

            else {
                System.out.println("Time's up!");
                timer.cancel(); //Terminate the timer thread
            }
        }
    }

    public static void main(String args[]) {
        System.out.println("About to schedule Reminder task in 5 seconds");
        new TimerReminder();
        System.out.println("Task scheduled.");
    }
}
