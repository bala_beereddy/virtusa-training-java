package gameDesign;

public interface Human extends Behaviours {
    //can set some final variables which are specific to human.
    public void humanspecific();
}
