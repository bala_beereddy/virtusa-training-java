package gameDesign;
/*
Continuing on our Massively Multiplayer Game Server development scenario, I have a new requirement in generating NPCs (Non-Playable Characters) in game play. Following is the high level requirement;



        I want to create both Human NPCs and Animal NPCs
        Human NPCs can be Villagers (men and women), soldiers (swordsmen, marksmen and spies). Animals can be Horses, Birds and Cattle
        Both Human and Animals can move (walk/run)


        My game needs to create these NPCs (Human and Animal) at the point of creating game environments (such as Stone-Age environment, Medieval Europe Environment etc.) using a Factory. To this factory, I should be able to tell exactly what type of NPCs I need.

        At the same time, I want to simulate panic situations (an enemy ambush, a sudden air strike etc.) and I want my NPCs (both Human and Animal) to flee from the affected area. For this also, I've decided a Factory would be a good idea.

        Note: It's advantages to keep the behaviour of mobility (walking, running, flying, swimming etc.) as an external concept coz it applies to Human NPCs, Animal NPCs, Vehicles and even weaponry.

        Your mission, if you chose to accept is;



        Create a OOP model to reflect the NPCs and the behaviour of mobility
        Create two factories that my Game Engine developers can use to create NPCs based on the above described scenarios.
*/
public class main {
    public static void main(String[] args) {
        stoneage stone = new stoneage(); //sample environment
        Swordsmen swordsmen=stone.createObj1(); //environment based swordsmen object
        marksmen marksmen=stone.createobj2(); //environment based marksmen object
    }
}
